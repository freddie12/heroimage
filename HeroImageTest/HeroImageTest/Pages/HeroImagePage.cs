﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Pages;

namespace HeroImageTest
{
	public class HeroImagePage : ContentPage
	{
		HeroImage heroImage;

		public HeroImagePage ()
		{
			Title = "HeroImageTest";

			heroImage = new HeroImage {
				ImageSource = "icon.png",
				BackgroundColor = Color.Black
			};

			heroImage.Text = "Text";
			heroImage.Detail = "Detail";
			heroImage.HeightRequest = 50;
			heroImage.WidthRequest = 50;
			heroImage.VerticalOptions = LayoutOptions.Start;
			heroImage.HorizontalOptions = LayoutOptions.Center;

			Content = heroImage; 
		}
	}
}

